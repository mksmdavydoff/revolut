package revolut.davydov.util

import java.text.DecimalFormatSymbols

class SymbolsRepo {
    val correctDelimeter by lazy {
        DecimalFormatSymbols.getInstance().decimalSeparator
    }
    val incorrectDelimeter by lazy {
        if (correctDelimeter == '.') ',' else '.'
    }
}