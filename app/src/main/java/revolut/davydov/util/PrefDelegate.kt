package revolut.davydov.util

import android.content.SharedPreferences
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

abstract class PrefDelegate<T>(
    private val prefs: SharedPreferences,
    private val id: String,
    private val defaultValue: T,
    private val afterChange: ((T) -> Unit)?
) :
    ReadWriteProperty<Any, T> {

    @Volatile
    var current: T? = null

    override fun getValue(thisRef: Any, property: KProperty<*>): T {
        return current ?: prefs.getValue(id, defaultValue)
    }

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        current = value
        prefs.edit().putValue(id, value).apply()
    }

    abstract fun SharedPreferences.Editor.putValue(id: String, value: T): SharedPreferences.Editor
    abstract fun SharedPreferences.getValue(id: String, defaultValue: T): T
}

class ListPrefDelegate(
    prefs: SharedPreferences,
    id: String,
    defaultValue: List<String>,
    afterChange: ((List<String>) -> Unit)? = null
) : PrefDelegate<List<String>>(prefs, id, defaultValue, afterChange) {
    override fun SharedPreferences.Editor.putValue(
        id: String,
        value: List<String>
    ) =
        putStringSet(id, value.toSet())

    override fun SharedPreferences.getValue(id: String, defaultValue: List<String>) =
        getStringSet(id, defaultValue.toSet()).toList()
}