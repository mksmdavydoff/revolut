package revolut.davydov.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import revolut.davydov.R
import revolut.davydov.logic.CurrencyInteractor
import revolut.davydov.util.SymbolsRepo
import revolut.davydov.util.optional

//in demo this screen was shown like tab, that's why i use fragment for it.
class CurrencyFragment : Fragment() {


    private var stopDisposable = CompositeDisposable()

    private lateinit var adapter: CurrencyAdapter
    private val currencyInteractor = CurrencyInteractor()
    private val symbolsRepo = SymbolsRepo()

    override fun onStart() {
        super.onStart()
        stopDisposable = CompositeDisposable()
        currencyInteractor.loadCurrenciesAmounts
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { adapter.setCurrentAmounts(it) }
            .beforeStop()
    }

    override fun onStop() {
        super.onStop()
        stopDisposable.dispose()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.currency_fragment, container)
        val recyclerView = view.findViewById<RecyclerView>(R.id.currency_rv)
        adapter = CurrencyAdapter(view.context, symbolsRepo, {
            recyclerView.scrollToPosition(0)
            currencyInteractor.setCurrencies(it)
        }) {
            currencyInteractor.setAmount(it)
        }
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(view.context)
        return view
    }


    private fun Disposable.beforeStop() {
        stopDisposable.add(this)
    }

}