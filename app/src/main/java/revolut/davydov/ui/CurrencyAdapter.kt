package revolut.davydov.ui

import android.content.Context
import android.text.*
import android.text.method.DigitsKeyListener
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import revolut.davydov.R
import revolut.davydov.util.SymbolsRepo

class CurrencyAdapter(
    context: Context,
    val symbolsRepo: SymbolsRepo,
    val currencyListListener: (List<String>) -> Unit,
    val amountListener: (String) -> Unit
) :
    RecyclerView.Adapter<CurrencyAdapter.CurrencyViewHolder>() {

    private var currenciesWithAmount = listOf<Pair<String, String>>()
    private val layoutInflater = LayoutInflater.from(context)

    fun setCurrentAmounts(
        newCurrenciesWithAmount: List<Pair<String, String>>
    ) {
        if (currenciesWithAmount.isEmpty()) {
            currenciesWithAmount = newCurrenciesWithAmount
            currencyListListener(currenciesWithAmount.map { it.first })
            notifyDataSetChanged()
        } else {
            val oldList = currenciesWithAmount
            currenciesWithAmount = newCurrenciesWithAmount
            DiffUtil.calculateDiff(object : DiffUtil.Callback() {
                override fun getOldListSize() = oldList.size

                override fun getNewListSize() = currenciesWithAmount.size

                override fun areContentsTheSame(
                    oldItemPosition: Int,
                    newItemPosition: Int
                ) =
                    oldList[oldItemPosition].second == currenciesWithAmount[newItemPosition].second
                            || oldItemPosition == 0 //we don't want to rebind 1 element

                override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                    oldList[oldItemPosition].first == currenciesWithAmount[newItemPosition].first

            }, false).dispatchUpdatesTo(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CurrencyViewHolder(
            layoutInflater.inflate(R.layout.currency_item, parent, false),
            symbolsRepo
        )

    override fun getItemCount() = currenciesWithAmount.size

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        val currencyWithAmount = currenciesWithAmount[position]
        holder.bindView(
            currencyWithAmount.first,
            currencyWithAmount.second,
            position == 0,
            ::onCurrencyClicked,
            ::onAmountChanged
        )
    }

    private fun onCurrencyClicked(currency: String) {
        val position =
            currenciesWithAmount.indexOfFirst { it.first == currency }
        if (position != 0) {
            currenciesWithAmount = currenciesWithAmount.toMutableList().apply {
                val replaced = removeAt(position)
                add(0, replaced)
            }
            notifyItemMoved(position, 0)
            notifyItemChanged(0)
            notifyItemChanged(1)
            currencyListListener(currenciesWithAmount.map { it.first })
        }


    }

    private fun onAmountChanged(amount: String) {
        amountListener(amount)
    }

    class CurrencyViewHolder(view: View, private val symbolsRepo: SymbolsRepo) :
        RecyclerView.ViewHolder(view) {
        private val nameView = view.findViewById<TextView>(R.id.currency_name_view)!!
        private val amountView = view.findViewById<EditText>(R.id.currency_amount_view)!!.apply {
            inputType = InputType.TYPE_CLASS_NUMBER
            keyListener =
                    DigitsKeyListener.getInstance(
                        "0123456789" +
                                symbolsRepo.correctDelimeter +
                                symbolsRepo.incorrectDelimeter
                    )
        }
        private var textWatcher: TextWatcher? = null

        fun bindView(
            currency: String,
            amount: String,
            canBeChanged: Boolean,
            currencyChangeListener: (String) -> Unit,
            amountChangeListener: (String) -> Unit
        ) {
            nameView.text = currency
            textWatcher?.let {
                amountView.removeTextChangedListener(it)
            }
            amountView.setText(amount)
            textWatcher = object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    amountChangeListener(s?.toString() ?: "")
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

            }
            amountView.addTextChangedListener(textWatcher)
            itemView.setOnClickListener {
                currencyChangeListener(currency)
            }
            if (canBeChanged) {
                with(amountView) {
                    isFocusable = true
                    isFocusableInTouchMode = true
                    filters = arrayOf(
                        DecimalDigitsInputFilter(
                            6,
                            2,
                            symbolsRepo
                        )
                    )
                    setOnClickListener(null)
                    requestFocus()
                }
            } else {
                with(amountView) {
                    setOnClickListener { currencyChangeListener(currency) }
                    isFocusable = false
                    isFocusableInTouchMode = false
                    filters = emptyArray() // sometimes results can contain more digits
                }
            }
        }
    }


}

class DecimalDigitsInputFilter(
    private val digitsBeforeDot: Int = 8,
    private val digitsAfterDot: Int = 2,
    private val symbolsRepo: SymbolsRepo
) :
    InputFilter {


    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        val newString = source.substring(start, end)
            .replace(symbolsRepo.incorrectDelimeter, symbolsRepo.correctDelimeter)
        val beforePart = dest.substring(0, dstart)
        val afterPart = dest.substring(dend, dest.length)

        val fullNewString = beforePart + newString + afterPart
        if (fullNewString.count { it == symbolsRepo.correctDelimeter } > 1) {
            return ""
        }
        val split = fullNewString.split(symbolsRepo.correctDelimeter)
        return when (split.size) {
            0 -> newString
            1 -> {
                if (isBeforeDotStringCorrect(split[0])) {
                    newString
                } else {
                    ""
                }
            }
            2 -> {
                if (isBeforeDotStringCorrect(split[0]) && isAfterDotStringCorrect(split[1])) {
                    newString
                } else {
                    ""
                }
            }
            else -> ""
        }
    }

    private fun isBeforeDotStringCorrect(beforeDotString: String) =
        (beforeDotString.length == 1 || !beforeDotString.startsWith('0')) && beforeDotString.length <= digitsBeforeDot

    private fun isAfterDotStringCorrect(afterDotString: String) =
        afterDotString.length <= digitsAfterDot
}




