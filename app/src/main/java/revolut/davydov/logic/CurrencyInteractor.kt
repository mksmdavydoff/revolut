package revolut.davydov.logic

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import revolut.davydov.models.CurrencyResponce
import revolut.davydov.util.Optional
import revolut.davydov.util.optional
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit

class CurrencyInteractor(
    currencyRepository: CurrencyRepository = CurrencyRepository(),
//    val symbolsRepo: SymbolsRepo = SymbolsRepo(),
    private val decimalFormat: DecimalFormat = DecimalFormat("#.##").apply {
        roundingMode = RoundingMode.HALF_UP
    }
) {

    private var currencyListSubject =
        BehaviorSubject.createDefault(listOf<String>())

    private var topCurrencyAmountSubject =
        BehaviorSubject.createDefault(Optional.empty<String>())

    private val loadRatesObservable =
        currencyListSubject
            .map { it.getOrNull(0).optional() }
            .distinctUntilChanged()
            .switchMap { opt ->
                Observable.interval(1, TimeUnit.SECONDS, Schedulers.io()).map { opt }
            }
            .switchMapMaybe {
                currencyRepository.getCurrencies(it.value)
                    .toMaybe()
                    .onErrorComplete()
            }.map {
                it.copy(rates = LinkedHashMap<String, Double>().apply {
                    put(it.base, 1.0)
                    putAll(it.rates)
                })
            }.observeOn(AndroidSchedulers.mainThread())


    val loadCurrenciesAmounts =
        Observable.combineLatest(
            topCurrencyAmountSubject,
            loadRatesObservable,
            BiFunction<Optional<String>, CurrencyResponce, Pair<Optional<String>, CurrencyResponce>> { amount, responce ->
                amount to responce
            })
            .withLatestFrom(currencyListSubject,
                BiFunction<Pair<Optional<String>, CurrencyResponce>, List<String>, List<Pair<String, String>>> { pair, currencies ->
                    val amount = pair.first
                    val responce = pair.second
                    val amountDouble = amount.parseDouble()
                    if (currencies.isEmpty()) {
                        responce.rates.map { it.key to (amountDouble * it.value).toCurrencyAmountString() }
                    } else {
                        currencies.mapNotNull { currency ->
                            responce.rates[currency]?.let { currency to (amountDouble * it).toCurrencyAmountString() }
                        }
                    }
                })

    fun setCurrencies(currencies: List<String>) {
        currencyListSubject.onNext(currencies)
    }

    fun setAmount(amount: String) {
        topCurrencyAmountSubject.onNext(amount.optional())
    }

    private fun Optional<String>.parseDouble() =
        value?.let {
            try {
                decimalFormat.parse(it).toDouble()
            } catch (ex: Exception) {
                null
            }
        } ?: 0.0

    private fun Double.toCurrencyAmountString() = decimalFormat.format(this)
}