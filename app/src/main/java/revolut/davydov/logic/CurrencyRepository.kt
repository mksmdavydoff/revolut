package revolut.davydov.logic

import com.google.gson.Gson
import io.reactivex.Single
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Request
import revolut.davydov.models.CurrencyResponce

class CurrencyRepository(
    private val client: OkHttpClient = OkHttpClient(),
    private val gson: Gson = Gson()
) {

    fun getCurrencies(currency: String?): Single<CurrencyResponce> {
        val call = getCurrenciesCall(currency)
        return Single.fromCallable {
            call.execute().body()?.charStream()?.let {
                gson.fromJson(
                    it,
                    CurrencyResponce::class.java
                )
            }!!
        }.doOnDispose { call.cancel() }
    }

    private fun getCurrenciesCall(base: String?) =
        client.newCall(Request.Builder().url(getUrl(base)).build())

    private fun getUrl(base: String?) = base?.let {
        BASE_URL.newBuilder().addQueryParameter("base", it).build()
    } ?: BASE_URL
}

private const val BASE_URL_STRING = "https://revolut.duckdns.org/latest"
private val BASE_URL = HttpUrl.parse(BASE_URL_STRING)!!