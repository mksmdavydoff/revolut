package revolut.davydov.models

data class CurrencyResponce(
    val base: String,
    val rates: LinkedHashMap<String, Double>
)